<?php

return array(
    /*
     * Here youe should add classnames of all created cachable repositories
     * to enable finding all related queries with any of these repositories, it allows to
     * automatically flush cache when model is creating, deleting or updating.
     */
    'repositories' => [

    ],
    /*
     * You can change repository resolver here to use your own if you need other caching mechanism
     */
    'resolver' => \Kromacie\L5Repository\RepositoryResolver::class,

    'cache' => [
        /*
         * Here is cache prefix for all created keys and tags. You should set something unique
         * to avoid overwriting your cache keys by other values. It may cause some unexpected errors.
         */
        'prefix' => 'l5repository',

        /*
         * Set the default store for all repositories
         * Remember, you can ovverride this in your repository or by setting this directly for method
         */
        'store' => 'redis',

        /*
         * Set the default time in minutes for all repositories. It cant be forever to not
         * choke your cache memory. Preffered time should be something like 10 minutes, becouse
         * if your database will grow up, it may fast overfill your memory.
         */
        'time' => 10,

        /*
         * Set if scopes from repositories should be cached as one of tag and part of builded cache key.
         * If you are using parameters in your scopes, it should be enabled. Otherwise, it will return
         * last cached query for your repository method regardless of set parameters.
         */
        'scopes' => true,

        /*
         * Set true if u want to enable caching, or false if u want to be disabled.
         * You can disable/enable this directly for repository or method.
         */
        'enabled' => true,
    ],

    /*
     * Here you can set directory destinations for console generated repositories and actions.
     */
    'generator' => [

        'destinations' => [
            /*
             * Model variable is a place where you store all your models.
             * It's necessary to generate complete path and predict model usage for repository
             */
            'models' => app_path('Models'),

            /*
             * This variable is a base directory where generated repositories should be stored
             */
            'repositories' => app_path('Repositories'),

            /*
             * This variable is a base directory where generated actions should be stored
             */
            'actions' => app_path('Actions'),
        ]
    ]
);