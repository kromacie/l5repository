<?php namespace Kromacie\L5Repository\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Kromacie\L5Repository\Generators\ActionGenerator;
use Kromacie\L5Repository\Generators\RepositoryGenerator;

class CreateRepositoryCommand extends Command
{
    use ResolvesNames;

    protected $signature = 'repository:create {name} {--cache} {--model=} {--crud} {--actions_dir=}';

    public function handle()
    {
        $options = array_filter(
            $this->options()
        );

        $model = Arr::get($options, 'model', false);
        $cache = Arr::has($options, 'cache');
        $crud = Arr::has($options, 'crud');
        $actions_dir = Arr::get($options, 'actions_dir', false);


        $this->getRepositoryGenerator()
            ->generate(
                $this->repositoryName(
                    $name = $this->argument('name')
                ), $model, $cache);

        if($crud) {
            $this->getActionGenerator()
                ->generate(
                    'Create',
                    $this->actionName($name, $actions_dir)
                );
            $this->getActionGenerator()
                ->generate(
                    'Update',
                    $this->actionName($name, $actions_dir)
                );
            $this->getActionGenerator()
                ->generate(
                    'Delete',
                    $this->actionName($name, $actions_dir)
                );
            $this->getActionGenerator()
                ->generate(
                    'Show',
                    $this->actionName($name, $actions_dir)
                );
            $this->getActionGenerator()
                ->generate(
                    'ShowAll',
                    $this->actionName($name, $actions_dir)
                );
        }
    }

    /**
     * @return RepositoryGenerator
     */
    public function getRepositoryGenerator()
    {
        return app(RepositoryGenerator::class);
    }

    /**
     * @return ActionGenerator
     */
    public function getActionGenerator()
    {
        return app(ActionGenerator::class);
    }
}