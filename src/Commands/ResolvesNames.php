<?php namespace Kromacie\L5Repository\Commands;

use Illuminate\Support\Str;

trait ResolvesNames
{
    public function repositoryName($name)
    {
        if(strpos($name, 'Repository') !== false) {
            return $name;
        }

        return Str::ucfirst($name . 'Repository');
    }

    public function actionName($name, $action_dir = false)
    {
        if($action_dir !== false) {
            return $action_dir;
        }

        if(strpos($name, 'Repository') !== false) {
            $name = str_replace('Repository', '', $name);
        }

        $regex = '/(?#! splitCamelCase Rev:20140412)
                # Split camelCase "words". Two global alternatives. Either g1of2:
                  (?<=[a-z])      # Position is after a lowercase,
                  (?=[A-Z])       # and before an uppercase letter.
                | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
                  (?=[A-Z][a-z])  # and before upper-then-lower case.
                /x';

        $partials = preg_split($regex, $name);

        return implode('\\', $partials);
    }
}