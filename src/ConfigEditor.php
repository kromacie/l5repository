<?php namespace Kromacie\L5Repository;

use Illuminate\Config\Repository;

class ConfigEditor
{
    private $name;
    private $sign;
    private $whitespace_offset;
    private $whitespace_variable;

    public function __construct($name, $whitespace_offset = 1, $whitespace_variable = 1, $sign = "\t")
    {
        $this->name = $name;
        $this->whitespace_offset = $whitespace_offset;
        $this->whitespace_variable = $whitespace_variable;
        $this->sign = $sign;
    }

    public function push($field, $variable)
    {
        $file = file_get_contents($this->name);

        $config = new Repository((include $this->name));

        $config = $config[$field];

        array_push($config, $variable);

        $config = $this->formatArrayItems($config);

        $toReplace = $this->prepareArrayString($config);

        $content = $this->replaceRepositoriesInContent($toReplace, $file, $field);

        $this->writeToFile($content);
    }

    private function formatArrayItems(array $repositories)
    {
        return array_map(function ($repository){
            return $this->generateGlobalWhitespaces() .
                $this->generateWhitespacesBeforeVariable()
                . "'" . $repository . "'";
        }, $repositories);
    }


    private function prepareArrayString(array $repositories)
    {
        return PHP_EOL . implode("," . PHP_EOL, $repositories) . PHP_EOL .
            $this->generateGlobalWhitespaces();
    }

    private function writeToFile($newContent)
    {
        file_put_contents($this->name, $newContent);
    }

    private function generateWhitespacesBeforeVariable()
    {
        return $this->generateChar($this->whitespace_variable, $this->sign);
    }

    private function generateGlobalWhitespaces()
    {
        return $this->generateChar($this->whitespace_offset, $this->sign);
    }

    private function generateChar($count, $char)
    {
        $str = '';

        for ($i = 0; $i < $count; $i++) {
            $str .= $char;
        }

        return $str;
    }

    private function replaceRepositoriesInContent($toReplace, $content, $field)
    {
        return preg_replace("/(\'{$field}\' \=\> \[)[\s\S]*?(\])/", "$1" .$toReplace. "$2", $content);
    }
}