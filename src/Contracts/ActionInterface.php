<?php namespace Kromacie\L5Repository\Contracts;

use Kromacie\L5Repository\Repositories\AbstractRepository;

interface ActionInterface
{
   public function perform(AbstractRepository $repository);
}