<?php namespace Kromacie\L5Repository\Contracts;

use Kromacie\L5Repository\Repositories\AbstractRepository;

interface ResolverInterface
{
    public function setConfig(array $config);
    public function resolve(AbstractRepository $repository, $method, $callback);
    public function isCachable(AbstractRepository $repository, $method);
    public function cache($callback, AbstractRepository $repository, $method);
    public function cacheHas(AbstractRepository $repository, $method);
    public function flushRelatedWith(AbstractRepository $repository);
    public function flushRepository(AbstractRepository $repository);
    public function flushRelation($class);
    public function flushAll();
}