<?php namespace Kromacie\L5Repository\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface ScopeInterface
{
    public function scope(Builder $builder);
}