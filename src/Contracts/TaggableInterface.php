<?php namespace Kromacie\L5Repository\Contracts;

interface TaggableInterface
{
    public function tags(): array ;
}