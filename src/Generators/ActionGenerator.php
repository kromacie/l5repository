<?php namespace Kromacie\L5Repository\Generators;

use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\NamespaceFileCreator;
use Kromacie\L5Repository\NamespaceResolver;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Nette\PhpGenerator\PhpFile;

class ActionGenerator
{
    private $namespace;
    private $creator;

    public function __construct(NamespaceResolver $namespace, NamespaceFileCreator $creator)
    {
        $this->creator = $creator;
        $this->namespace = $namespace;
    }

    protected function generateClass($classname, $namespace)
    {
        $file = new PhpFile();
        $namespace = $file->addNamespace($namespace);

        $class = $namespace->addClass($classname);
        $class->addImplement(ActionInterface::class);
        $namespace->addUse(ActionInterface::class);
        $namespace->addUse(AbstractRepository::class);

        $class->addMethod('__construct')
            ->setComment("{$classname} constructor");

        $class->addMethod('perform')
            ->setVisibility('public')
            ->addParameter('repository')
                ->setTypeHint(AbstractRepository::class);

        return $file;
    }

    private function getNamespaceWithPrefix($namespace, $prefix = false)
    {
        return $prefix ?
            $namespace . '\\' . trim(str_replace('/', '\\', $prefix), '\\ ') :
            $namespace;
    }

    public function generate($class, $prefix_dir = false)
    {
        $actionNamespace = $this->namespace->forAction($class, $actionName);

        return $this->creator->create(
            $actionName,
            $this->getNamespaceWithPrefix($actionNamespace, $prefix_dir),
            $this->generateClass($actionName,
                $this->getNamespaceWithPrefix($actionNamespace, $prefix_dir)
            )
        );
    }
}