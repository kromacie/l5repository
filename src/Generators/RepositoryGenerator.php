<?php namespace Kromacie\L5Repository\Generators;

use Kromacie\L5Repository\ConfigEditor;
use Kromacie\L5Repository\NamespaceFileCreator;
use Kromacie\L5Repository\NamespaceResolver;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Traits\CachableRepository;
use Nette\PhpGenerator\PhpFile;

class RepositoryGenerator
{
    private $namespace;
    private $creator;
    private $editor;

    public function __construct(NamespaceResolver $namespace, NamespaceFileCreator $creator, ConfigEditor $editor)
    {
        $this->creator = $creator;
        $this->namespace = $namespace;
        $this->editor = $editor;
    }

    protected function generateClass($class, $namespace, $model = false, $cacheTrait = false)
    {
        $file = new PhpFile();
        $namespace = $file->addNamespace($namespace);

        $class = $namespace->addClass($class);
        $class->addExtend(AbstractRepository::class);
        $namespace->addUse(AbstractRepository::class);

        if($cacheTrait) {
            $class->addTrait(CachableRepository::class);
            $namespace->addUse(CachableRepository::class);

            $class->addMethod('getCachableMethods')
                ->setReturnType('array')
                ->setVisibility('public')
                ->setBody('return [];');
        }

        $modelAlias = 'null';

        if($model) {
            $modelAlias = explode('\\', $model);
            $modelAlias = array_pop($modelAlias);
            $modelAlias .= '::class';
            $namespace->addUse($model);
        }

        $class->addMethod('getClass')
            ->setVisibility('public')
            ->setStatic(true)
            ->setReturnType('string')
            ->setBody("return $modelAlias;");

        return $file;
    }

    public function generate($class, $model = false, $cacheTrait = false)
    {
        if($model) {
            $model = $this->namespace->forModel($model, $modelName) . '\\' . $modelName;
        }

        $repositoryNamespace = $this->namespace->forRepository($class, $repositoryName);

        $this->editor->push('repositories', $repositoryNamespace . '\\' . $repositoryName);

        return $this->creator->create(
            $repositoryName,
            $repositoryNamespace,
            $this->generateClass($repositoryName, $repositoryNamespace, $model, $cacheTrait)
        );
    }
}