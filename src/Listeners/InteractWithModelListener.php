<?php namespace Kromacie\L5Repository\Listeners;

use Illuminate\Database\Eloquent\Model;
use Kromacie\L5Repository\Contracts\ResolverInterface;

class InteractWithModelListener
{

    private $resolver;

    public function __construct(ResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    public function deleted(Model $model)
    {
        $this->resolver->flushRelation(get_class($model));
    }

    public function created(Model $model)
    {
        $this->resolver->flushRelation(get_class($model));
    }

    public function updated(Model $model)
    {
        $this->resolver->flushRelation(get_class($model));
    }
}