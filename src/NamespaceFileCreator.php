<?php namespace Kromacie\L5Repository;

class NamespaceFileCreator
{
    private $app_path;
    private $prefix;
    private $ext;

    /**
     * NamespaceFileCreator constructor.
     * @param $app_path
     * @param string $prefixed
     * @param string $ext
     */
    public function __construct($app_path, $prefixed = 'App', $ext = '.php')
    {
        $this->app_path = $app_path;
        $this->prefix = trim(str_replace('/', '\\', $prefixed));
        $this->ext = $ext;
    }

    public function create($filename, $namespace, $content)
    {
        $this->createDirectoryIfNotExists(
            $directory = $this->predictDirectoryForNamespace($namespace)
        );

        $this->createFile(
            $file = $this->createStaticFilename($filename, $directory), $content
        );

        return $file;
    }

    private function createDirectoryIfNotExists($directory)
    {
        $directory = str_replace('/', '\\', $directory);
        $base = str_replace('/', '\\', $this->app_path);

        $relativeDir = str_replace($base, '', $directory);

        $relativeDirParts = explode('\\', $relativeDir);

        $searched = $base;

        foreach ($relativeDirParts as $part) {
            $searched .= '\\' . $part;
            if(!file_exists($searched)) {
                mkdir($searched);
            }
        }
    }

    private function createFile($filePath, $content)
    {
        file_put_contents($filePath, $content);
    }

    private function predictDirectoryForNamespace($namespace)
    {
        $namespaceParts = explode('\\', $namespace);

        // Remove Prefix parts from namespace
        array_splice($namespaceParts, 0, count(explode('\\', $this->prefix)));

        return $this->getApplicationPath() . '\\' . implode('\\', $namespaceParts);
    }

    private function createStaticFilename($name, $directory)
    {
        $ext = $this->ext;
        if(($pos = strpos($this->ext, '.')) === false && $this->ext === null) {
            $ext = '';
        } elseif($pos !== 0) {
            $ext = '.' . $this->ext;
        }
        return $directory . '\\' . trim($name) . $ext;
    }

    private function getApplicationPath()
    {
        return $this->app_path ? $this->app_path : app_path();
    }
}