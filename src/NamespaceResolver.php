<?php namespace Kromacie\L5Repository;

class NamespaceResolver
{
    private $repositories_dir;
    private $actions_dir;
    private $models_dir;
    private $app_path;
    private $prefix;

    public function __construct($repositories_dir, $actions_dir, $models_dir, $app_path = null, $prefix = 'App')
    {
        $this->repositories_dir = $repositories_dir;
        $this->actions_dir = $actions_dir;
        $this->models_dir = $models_dir;
        $this->app_path = $app_path;
        $this->prefix = trim(str_replace('/', '\\', $prefix), '\\ ');
    }

    public function forRepository($name, &$suggested_classname)
    {
        return $this->resolve($name, $this->repositories_dir, $suggested_classname);
    }

    public function forAction($name, &$suggested_classname)
    {
        return $this->resolve($name, $this->actions_dir, $suggested_classname);
    }

    public function forModel($name, &$suggested_classname)
    {
        return $this->resolve($name, $this->models_dir, $suggested_classname);
    }

    protected function resolve($name, $directory, &$suggested_classname)
    {
        $name = $this->normalize($name);

        $base_namespace = $this->getBaseNamespace($directory);

        $name = trim($name, '\\');

        $full_name = $base_namespace . '\\' . $name;

        $full_name_parts = explode('\\', $full_name);

        $suggested_classname = array_pop($full_name_parts);

        return implode('\\', $full_name_parts);
    }

    private function normalize($name)
    {
        return trim(str_replace('/', '\\', $name), "\\ ");
    }

    private function getBaseNamespace($directory)
    {
        $full_dir = $this->normalize($directory);
        $base_dir = $this->normalize($this->getApplicationPath());
        $additional_namespace = str_replace($base_dir, '', $full_dir);
        return ($this->prefix ? $this->prefix : '') . $additional_namespace;
    }

    private function getApplicationPath()
    {
        return $this->app_path ? $this->app_path : app_path();
    }
}