<?php namespace Kromacie\L5Repository\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Contracts\ResolverInterface;
use Kromacie\L5Repository\Contracts\ScopeInterface;
use Kromacie\L5Repository\Scopes\ScopePagination;

abstract class AbstractRepository
{

    protected $scopes;

    protected $resolver;

    public function __construct(ResolverInterface $resolver)
    {
        $this->scopes = [];

        $this->resolver = $resolver;
    }

    abstract public static function getClass(): string;

    public function flushScopes()
    {
        $this->scopes = [];
    }

    /**
     * @param ScopeInterface|\Closure ...$scopes
     * @return $this
     */
    public function scope(...$scopes)
    {
        foreach ($scopes as $scope) {
            $this->scopes[] = ($scope);
        }

        return $this;
    }

    protected function applyScopes(Builder $builder)
    {
        /** @var ScopeInterface $scope */
        foreach ($this->scopes as $scope)
        {
            if($scope instanceof ScopeInterface) {
                $scope->scope($builder);
            }

            if($scope instanceof \Closure) {
                call_user_func($scope, $builder);
            }
        }
    }

    public function getModel(): Model
    {
        return app()->make($this->getClass());
    }

    public function getScopes()
    {
        return $this->scopes;
    }

    public function get(array $columns = ['*'])
    {
        $query = $this->newQueryAndApplyScopes();

        return $query->get($columns);
    }

    public function first(array $columns = ['*'])
    {
        $query = $this->newQueryAndApplyScopes();

        return $query->first($columns);
    }

    public function delete()
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->delete();
    }

    public function updateOrCreate(array $attributes)
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->updateOrCreate($attributes);
    }

    public function updateOrInsert(array $attributes)
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->updateOrInsert($attributes);
    }

    public function update(array $values)
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->update($values);
    }

    public function create(array $attributes)
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->create($attributes);
    }

    public function insert(array $values)
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->insert($values);
    }

    public function insertGetId(array $values, string $sequence = null)
    {
        $query = $this->newQueryAndApplyScopes(true);

        return $query->insertGetId($values, $sequence);
    }

    public function sum(string $column)
    {
        $query = $this->newQueryAndApplyScopes();

        return $query->sum($column);
    }

    public function count(string $columns = '*')
    {
        $query = $this->newQueryAndApplyScopes();

        return $query->count($columns);
    }

    public function exists()
    {
        $query = $this->newQueryAndApplyScopes();

        return $query->exists();
    }

    public function paginate(int $perPage = null, array $columns = ['*'], string $pageName = null, int $page = null): LengthAwarePaginator
    {
        $query = $this->getModel()->newQuery();

        $this->scope(new ScopePagination($perPage, $pageName, $page));

        $this->applyScopes($query);

        return $query->paginate($perPage, $columns, $pageName, $page);
    }

    public function perform(ActionInterface $action)
    {
        return $action->perform($this);
    }

    private function newQueryAndApplyScopes($flush = false)
    {
        $query = $this->getModel()->newQuery();

        $this->applyScopes($query);

        if($flush) {
            $this->resolver->flushRelatedWith($this);
        }

        $this->flushScopes();

        return $query;
    }

    protected function getResolver()
    {
        return $this->resolver;
    }
}