<?php namespace Kromacie\L5Repository;

use Exception;
use Illuminate\Cache\CacheManager;
use Illuminate\Cache\TaggableStore;
use Illuminate\Cache\TaggedCache;
use Kromacie\L5Repository\Contracts\ResolverInterface;
use Kromacie\L5Repository\Exceptions\ConfigNotFoundException;
use Kromacie\L5Repository\Exceptions\NotCachableException;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Traits\CachableRepository;

class RepositoryResolver implements ResolverInterface
{

    private $config;

    private $cache;

    public function __construct(CacheManager $cache)
    {
        $this->cache = $cache;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param AbstractRepository $repository
     * @param $method
     * @param $callback
     * @return mixed
     * @throws ConfigNotFoundException
     */
    public function resolve(AbstractRepository $repository, $method, $callback)
    {
        if($this->isCachable($repository, $method)) {
            $result = $this->cache($callback, $repository, $method);
            $repository->flushScopes();

            return $result;
        }

        return $callback();

    }

    public function isCachable(AbstractRepository $repository, $method)
    {

        if($method == false) {
            return false;
        }

        if(!in_array(CachableRepository::class, class_uses($repository))) {
            return false;
        }

        foreach ($repository->getScopes() as $scope)
        {
            if($scope instanceof \Closure) {
                return false;
            }
        }

        /** @var CachableRepository $repository */
        try {
            $config = $repository->getConfig($method);

            return $config['enabled'];

        } catch (ConfigNotFoundException $exception) {
            return false;
        }

    }

    /**
     * @param AbstractRepository $repository
     * @param $method
     * @return bool
     * @throws Exception
     */
    public function cacheHas(AbstractRepository $repository, $method)
    {

        if(!in_array(CachableRepository::class, class_uses($repository))) {
            return false;
        }

        /** @var CachableRepository $repository */
        if(!$repository->hasMethod($method)) {
            return false;
        }

        $cache = $this->getCache($repository, $method);

        $vars = $this->buildVars($repository, $method);

        $key = $this->buildCacheKey(...$vars);

        /** @var TaggedCache $cache */
        return $cache->has($key);

    }

    /**
     * @param $callback
     * @param AbstractRepository $repository
     * @param $method
     * @return mixed
     * @throws ConfigNotFoundException
     */
    public function cache($callback, AbstractRepository $repository, $method)
    {
        $cache = $this->getCache($repository, $method);

        $vars = $this->buildVars($repository, $method);

        $key = $this->buildCacheKey(...$vars);

        /** @var CachableRepository $repository */
        $time = $repository->getConfig($method)["cacheTime"];

        /** @var TaggedCache $cache */
        return $cache->remember($key, $time, $callback);
    }

    /**
     * @param AbstractRepository $repository
     * @throws NotCachableException
     * @throws ConfigNotFoundException
     */
    public function flushRepository(AbstractRepository $repository)
    {

        if(!in_array(CachableRepository::class, class_uses($repository))) {
            throw new NotCachableException("Repository is not cachable!");
        }

        /** @var CachableRepository $repository */
        $config = $repository->buildConfig();

        foreach ($config as $method => $value) {

            /** @var TaggableStore $cache */
            $cache = $this->cache->driver($this->getCacheStore($repository, $method));

            $cache->tags($this->buildTag(get_class($repository), $method))->flush();

        }
    }

    /**
     * @throws Exception
     */
    public function flushAll()
    {
        foreach ($this->getRepositories() as $class) {
            $repository = app()->make($class);
            if(in_array(CachableRepository::class, class_uses($repository))) {
                $this->flushRepository($repository);
            }
        }
    }

    /**
     * @param $relation
     * @throws Exception
     */
    public function flushRelation($relation)
    {
        foreach ($this->getRepositories() as $class) {

            $repository = app()->make($class);

            if(in_array(CachableRepository::class, class_uses($repository))) {
                $config = $repository->buildConfig();

                foreach ($config as $method => $value) {
                    /** @var TaggableStore $cache */
                    $cache = $this->cache->driver($this->getCacheStore($repository, $method));

                    $cache->tags($this->buildTag(get_class($repository), $relation))->flush();

                }
            }
        }
    }

    /**
     * @param AbstractRepository $repository
     * @throws Exception
     */
    public function flushRelatedWith(AbstractRepository $repository)
    {
        $this->flushRelation($repository::getClass());

        if(in_array(CachableRepository::class, class_uses($repository))) {
            /** @var CachableRepository $repository */
            $tags = $repository->getTags();

            foreach ($tags as $tag) {
                $this->flushRelation($tag);
            }
        }
    }

    /**
     * @param AbstractRepository $repository
     * @param $method
     * @return array
     * @throws ConfigNotFoundException
     */
    private function buildVars(AbstractRepository $repository, $method)
    {
        /** @var AbstractRepository|CachableRepository $repository */
        $config = $repository->getConfig($method);

        $cacheScopes = $config["cacheScopes"];

        $vars = [];

        $vars[] = $repository;
        $vars[] = $method;

        if($cacheScopes) {
            $vars[] = $repository->getScopes();
        }

        return $vars;
    }

    /**
     * @param $value
     * @param bool $optional
     * @return string
     * @throws ConfigNotFoundException
     */
    private function buildTag($value, $optional = false)
    {
        $prefix = $this->getCachePrefix();

        $tag = $optional ? "$prefix.$value.$optional" : "$prefix.$value";

        return $tag;
    }

    /**
     * @param AbstractRepository $repository
     * @param $method
     * @param array $scopes
     * @return string
     * @throws ConfigNotFoundException
     */
    private function buildCacheKey(AbstractRepository $repository, $method, array $scopes = [])
    {

        $class = $repository::getClass();

        return  $this->buildTag($class) . '@' .
                $this->buildTag($method) . ':' .
                $this->buildTag(serialize($scopes));

    }

    /**
     * @param AbstractRepository $repository
     * @param $method
     * @param bool $cacheScopes
     * @return TaggedCache
     * @throws ConfigNotFoundException
     */
    private function getCache(AbstractRepository $repository, $method, bool $cacheScopes = true)
    {
        /** @var AbstractRepository|CachableRepository $repository */
        $config = $repository->getConfig($method);

        $store = $config["store"];

        /** @var TaggableStore $cache */
        $cache = $this->cache->driver($store);

        $tags = [];

        if($cacheScopes) {
            foreach ($repository->getTags() as $tag) {
                $tags[] = $this->buildTag(get_class($repository), $tag);
            }
        }

        $tags[] = $this->buildTag(get_class($repository), $method);
        $tags[] = $this->buildTag(get_class($repository), $repository::getClass());
        $tags[] = $this->buildTag(get_class($repository));

        return $cache->tags($tags);
    }

    /**
     * @param AbstractRepository $repository
     * @param $method
     * @return mixed
     * @throws ConfigNotFoundException
     */
    public function getCacheStore(AbstractRepository $repository, $method)
    {
        /** @var AbstractRepository|CachableRepository $repository */
        $config = $repository->getConfig($method);

        return $config["store"];
    }

    /**
     * @return mixed
     * @throws ConfigNotFoundException
     */
    private function getConfig()
    {
        if(!$config = $this->config) {
            throw new ConfigNotFoundException("Missing configuration!");
        }

        return $config;
    }

    /**
     * @return mixed
     * @throws ConfigNotFoundException
     */
    private function getCachePrefix()
    {
        $config = $this->getConfig();

        return $config["cache"]["prefix"];
    }

    /**
     * @return mixed
     * @throws ConfigNotFoundException
     */
    private function getRepositories()
    {
        $config = $this->getConfig();

        return $config["repositories"];
    }

}