<?php namespace Kromacie\L5Repository;

use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Kromacie\L5Repository\Commands\CreateRepositoryCommand;
use Kromacie\L5Repository\Contracts\ResolverInterface;
use Kromacie\L5Repository\Generators\ActionGenerator;
use Kromacie\L5Repository\Generators\RepositoryGenerator;
use Kromacie\L5Repository\Listeners\InteractWithModelListener;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerConfig();
        $this->registerObserver();
    }

    public function register()
    {
        $this->registerBindings();
        $this->registerCommands();
    }

    public function registerBindings()
    {
        $this->app->singleton(ResolverInterface::class, function (Container $app) {
            /** @var ResolverInterface $resolver */
            $resolver = config('l5repository.resolver');
            $resolver = new $resolver(
                $app['cache']
            );
            $resolver->setConfig(config('l5repository'));
            return $resolver;
        });

        $this->registerActionGenerator();
        $this->registerConfigEditor();
        $this->registerNamespaceFileCreator();
        $this->registerNamespaceResolver();

    }

    public function registerCommands()
    {
        $this->commands([
            CreateRepositoryCommand::class
        ]);
    }

    public function registerConfigEditor()
    {
        $this->app->singleton(ConfigEditor::class, function (){
            return new ConfigEditor(config_path(), 1, 2, "\t");
        });
    }

    public function registerNamespaceFileCreator()
    {
        $this->app->singleton(NamespaceFileCreator::class, function () {
            return new NamespaceFileCreator(app_path());
        });
    }

    public function registerRepositoryGenerator()
    {
        $this->app->singleton(RepositoryGenerator::class, function (){
            return new RepositoryGenerator(
                $this->app->get(NamespaceResolver::class),
                $this->app->get(NamespaceFileCreator::class),
                $this->app->get(ConfigEditor::class)
            );
        });
    }

    public function registerActionGenerator()
    {
        $this->app->singleton(ActionGenerator::class, function (Container $app){
            return new ActionGenerator(
                $app->get(NamespaceResolver::class),
                $app->get(NamespaceFileCreator::class)
            );
        });
    }

    public function registerNamespaceResolver()
    {
        $this->app->singleton(NamespaceResolver::class, function (Container $app) {
            $dirs = config('l5repository.generator.destinations');

            return new NamespaceResolver($dirs['repositories'], $dirs['actions'], $dirs['models'], app_path(), 'App');
        });
    }

    public function registerConfig()
    {
        if(!$this->isLumen()) {
            $this->publishes([
                __DIR__.'/../config/l5repository.php' => config_path('l5repository.php'),
            ], 'l5repositories');
        }

        $this->mergeConfigFrom( __DIR__.'/../config/l5repository.php', 'l5repository');

    }

    public function registerObserver()
    {
        $repositories = $this->app['config']['l5repository.repositories'];

        /** @var AbstractRepository $repository */
        foreach ($repositories as $repository) {
            /** @var Model $model */
            $model = $repository::getClass();
            $model::observe(InteractWithModelListener::class);
        }
    }

    public function isLumen()
    {
        if (app() instanceof \Illuminate\Foundation\Application) {
            return false;
        } else {
            return true;
        }
    }

}