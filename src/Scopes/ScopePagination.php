<?php namespace Kromacie\L5Repository\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class ScopePagination implements ScopeInterface
{

    private $perPage;
    private $pageName;
    private $page;

    public function __construct(int $perPage = null, string $pageName = null, int $page = null)
    {
        $this->perPage = $perPage;
        $this->pageName = $pageName;
        $this->page = $page;
    }

    public function scope(Builder $builder)
    {
        // TODO: Implement scope() method.
    }
}