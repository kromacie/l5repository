<?php namespace Kromacie\L5Repository\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Scope;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Exceptions\ConfigNotFoundException;
use Kromacie\L5Repository\Exceptions\RuntimeException;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\RepositoryResolver;
use Kromacie\L5Repository\Contracts\TaggableInterface;
use Kromacie\L5Repository\Scopes\ScopePagination;
use ReflectionClass;

trait CachableRepository
{

    protected $cacheScopes;

    protected $store;

    protected $time;

    protected $enabled;

    private $temporaryConfig = [];

    abstract public function getCachableMethods(): array;

    public function getDefaultStore() {
        return is_null($this->store) ? config('l5repository.cache.store') : $this->store;
    }

    /**
     * Return cache time in minutes
     * @return int
     */
    public function getDefaultTime() {
        return is_null($this->time) ? config('l5repository.cache.time') : $this->time;
    }

    public function getDefaultEnabled() {
        return is_null($this->enabled) ? config('l5repository.cache.enabled') : $this->enabled;
    }

    public function cacheScopesByDefault() {
        return is_null($this->cacheScopes) ? config('l5repository.cache.scopes') : $this->cacheScopes;
    }

    /**
     * @param $method
     * @return mixed
     * @throws ConfigNotFoundException
     */
    public function getConfig($method)
    {
        $config = $this->buildConfig();

        if($this->hasMethod($method)) {
            return $config[$method];
        }

        throw new ConfigNotFoundException("Excepted method not exists!");
    }

    public function hasMethod($method)
    {
        $config = $this->buildConfig();

        return key_exists($method, $config);
    }

    /**
     * @param $key
     * @param null $value
     * @return bool
     * @throws RuntimeException
     */
    public function runtime($key, $value = null)
    {

        $method = $this->getCalledMethod();

        if (!$method) {
            throw new RuntimeException("This should'nt be called outside of Action or Repository!");
        }

        try {
            $config = $this->getConfig($method);
        } catch (ConfigNotFoundException $e) {
            return null;
        }

        if(is_null($value)) {
            return $config[$key];
        }

        $this->temporaryConfig[$key] = $value;

        return null;
    }

    public function buildConfig()
    {
        $all = [];

        $methods = $this->getCachableMethods();

        foreach ($methods as $key => $value) {

            if(!is_numeric($key)) {

                if(!is_array($value)) {
                    $value = $this->getDefaultConfig();
                }

                $all[$key] = array_replace_recursive($this->getDefaultConfig(), $value, $this->temporaryConfig);

            } else {
                $all[$value] = array_replace_recursive($this->getDefaultConfig(), $this->temporaryConfig);
            }
        }

        return $all;
    }

    private function getDefaultConfig()
    {
        return [
            'store' => $this->getDefaultStore(),
            'cacheScopes' => $this->cacheScopesByDefault(),
            'cacheTime' => $this->getDefaultTime(),
            'enabled' => $this->getDefaultEnabled(),
        ];
    }

    public function getTags()
    {
        $tags = [];

        /** @var Scope $scope */
        foreach ($this->getScopes() as $scope) {
            if($scope instanceof TaggableInterface) {
                $tags = array_merge($tags, $scope->tags());
            }
        }
        return $tags;
    }

    /**
     * @param int $level
     * @return mixed
     */
    private function getCalledMethod($level = 2)
    {
        $result =  debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, $level + 1)[$level];

        try {
            $class = new ReflectionClass($result['class']);
        } catch (\ReflectionException $exception) {
            return false;
        }

        if($class->isSubclassOf(AbstractRepository::class)) {
            return $result['function'];
        } elseif ($class->implementsInterface(ActionInterface::class)) {
            return $result['class'];
        } else {
            return false;
        }
    }

    private function clearTemporaryConfig() {
        $this->temporaryConfig = [];
    }

    public function get(array $parameters = ['*'])
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();

        $method = $this->getCalledMethod();

        return $resolver->resolve($this, $method, function () use ($parameters){
            $this->clearTemporaryConfig();
            return parent::get($parameters);
        });
    }

    public function first(array $parameters = ['*'])
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();

        $method = $this->getCalledMethod();

        return $resolver->resolve($this, $method, function () use ($parameters){
            $this->clearTemporaryConfig();
            return parent::first($parameters);
        });
    }

    public function sum(string $column)
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();

        $method = $this->getCalledMethod();

        return $resolver->resolve($this, $method, function () use ($column){
            $this->clearTemporaryConfig();
            return parent::sum($column);
        });
    }

    /**
     * @param string $columns
     * @return mixed
     * @throws ConfigNotFoundException
     * @throws RuntimeException
     */
    public function count(string $columns = '*')
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();

        $method = $this->getCalledMethod();

        return $resolver->resolve($this, $method, function () use ($columns){
            $this->clearTemporaryConfig();
            return parent::count($columns);
        });
    }

    public function exists()
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();

        $method = $this->getCalledMethod();

        return $resolver->resolve($this, $method, function (){
            $this->clearTemporaryConfig();
            return parent::exists();
        });
    }

    /**
     * @param int|null $perPage
     * @param array $columns
     * @param string $pageName
     * @param int|null $page
     * @return LengthAwarePaginator
     * @throws RuntimeException
     * @throws ConfigNotFoundException
     */
    public function paginate(int $perPage = null, array $columns = ['*'], string $pageName = null, int $page = null): LengthAwarePaginator
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();

        $method = $this->getCalledMethod();

        $this->scope(new ScopePagination($perPage, $pageName, $page));

        return $resolver->resolve($this, $method, function () use ($perPage, $columns, $pageName, $page){
            $this->flushScopes();
            $this->clearTemporaryConfig();
            return parent::paginate($perPage, $columns, $pageName, $page);
        });
    }

    public function flush()
    {
        /** @var RepositoryResolver $resolver */
        $resolver = $this->getResolver();
        $resolver->flushRepository($this);
    }



}