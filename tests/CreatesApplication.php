<?php namespace Kromacie\L5Repository\Tests;

use Dotenv\Dotenv;
use Exception;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\App;
use Kromacie\L5Repository\RepositoryServiceProvider;
use Kromacie\L5Repository\Tests\Fixtures\Article;
use Kromacie\L5Repository\Tests\Fixtures\Repository\ArticleRepository;
use Kromacie\L5Repository\Tests\Fixtures\Repository\WalletRepository;
use Kromacie\L5Repository\Tests\Fixtures\Wallet;
use PDO;

trait CreatesApplication
{

    use DatabaseMigrations;

    public function setUp() : void
    {
        parent::setUp();

        $this->withFactories(__DIR__ . '/database/factories');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        factory(Article::class, 10)->create();
        factory(Wallet::class, 10)->create();

    }

    public function getPackageProviders($app)
    {
        return [
            RepositoryServiceProvider::class
        ];
    }

    protected function getEnvironmentSetUp($app)
    {

        $filename = __DIR__.'/../';

        try {
            Dotenv::create($filename, '.env.testing')->load();
        } catch (Exception $e) {

        }

        $app['config']->set('database.default', 'test');
        $app['config']->set('database.connections.test', [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'test'),
            'username' => env('DB_USERNAME', 'root'),
            'password' => env('DB_PASSWORD', 'root'),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ]);

        $app['config']->set('database.redis.default', [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 1,
        ]);

        $app['config']->set('cache.stores.redis', [
            'driver' => 'redis',
            'connection' => 'default',
        ]);

        $app['config']->set('l5repository.repositories', [
            WalletRepository::class,
            ArticleRepository::class
        ]);

        copy(__DIR__ . '/../config/l5repository.php', __DIR__ . '/config.php');
    }

    /*
     * php delete function that deals with directories recursively
     */
    public function deleteDirectory($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file ){
                $this->deleteDirectory( $file );
            }

            rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );
        }
    }

    protected function tearDown(): void
    {
        unlink(__DIR__  . '/config.php');
        $this->deleteDirectory(__DIR__ . '/Fixtures/Generated/');
    }


}