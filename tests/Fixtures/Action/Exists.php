<?php namespace Kromacie\L5Repository\Tests\Fixtures\Action;

use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;

class Exists implements ActionInterface
{

    public function perform(AbstractRepository $repository)
    {
        return $repository->exists();
    }
}