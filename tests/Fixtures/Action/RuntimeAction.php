<?php namespace Kromacie\L5Repository\Tests\Fixtures\Action;

use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Traits\CachableRepository;

class RuntimeAction implements ActionInterface
{

    /**
     * @param CachableRepository $repository
     */
    public function perform(AbstractRepository $repository)
    {
        if($repository->runtime('enabled')) {
            $repository->runtime('enabled', false);
        }
        return $repository->get();
    }
}