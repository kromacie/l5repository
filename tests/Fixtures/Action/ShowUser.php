<?php namespace Kromacie\L5Repository\Tests\Fixtures\Action;

use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Tests\Fixtures\Scope\IDScope;

class ShowUser implements ActionInterface
{

    /**
     * @param AbstractRepository $repository
     */
    public function perform(AbstractRepository $repository)
    {
        $repository->scope(new IDScope(1));

        return $repository->first(['*']);
    }

}
