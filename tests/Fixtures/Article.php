<?php namespace Kromacie\L5Repository\Tests\Fixtures;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'title', 'description'
    ];
}