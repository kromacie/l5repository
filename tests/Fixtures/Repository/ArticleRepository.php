<?php namespace Kromacie\L5Repository\Tests\Fixtures\Repository;

use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Tests\Fixtures\Article;
use Kromacie\L5Repository\Traits\CachableRepository;

class ArticleRepository extends AbstractRepository
{
    use CachableRepository;


    public static function getClass(): String
    {
        return Article::class;
    }

    public function getCachableMethods(): array
    {
        return [
            'getAll'
        ];
    }

    public function getAll()
    {
        return $this->get();
    }
}