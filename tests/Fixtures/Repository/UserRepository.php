<?php namespace Kromacie\L5Repository\Tests\Fixtures\Repository;

use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Tests\Fixtures\User;

class UserRepository extends AbstractRepository
{

    public static function getClass(): String
    {
        return User::class;
    }
}