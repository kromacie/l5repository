<?php namespace Kromacie\L5Repository\Tests\Fixtures\Repository;

use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\Tests\Fixtures\Action\Count;
use Kromacie\L5Repository\Tests\Fixtures\Action\Exists;
use Kromacie\L5Repository\Tests\Fixtures\Action\RuntimeAction;
use Kromacie\L5Repository\Tests\Fixtures\Action\Sum;
use Kromacie\L5Repository\Tests\Fixtures\Scope\IDScope;
use Kromacie\L5Repository\Tests\Fixtures\Scope\WithUsers;
use Kromacie\L5Repository\Tests\Fixtures\Wallet;
use Kromacie\L5Repository\Traits\CachableRepository;

class WalletRepository extends AbstractRepository
{
    use CachableRepository;

    public static function getClass(): String
    {
        return Wallet::class;
    }

    public function getCachableMethods(): array
    {
        return [
            'showWallet',
            'showWalletWithUser' => [
                'enabled' => true
            ],
            'showSite',
            RuntimeAction::class,
            Count::class,
            Exists::class,
            Sum::class
        ];
    }

    public function showWallet()
    {
        $this->scope(new IDScope(1));

        return $this->first();
    }

    public function showWalletWithUser()
    {
        $this->scope(new IDScope(1));
        $this->scope(new WithUsers());

        return $this->first();
    }

    public function showSite($site) {
        return $this->paginate(2, ['*'], 'page', $site);
    }

}
