<?php namespace Kromacie\L5Repository\Tests\Fixtures\Scope;



use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class EmailScope implements ScopeInterface
{

    private $email;

    /**
     * EmailScope constructor.
     * @param $email
     */
    public function __construct($email)
    {
        $this->email = $email;
    }


    public function scope(Builder $builder)
    {
        $builder->where('email', '=', $this->email);
    }
}