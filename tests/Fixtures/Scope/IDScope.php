<?php namespace Kromacie\L5Repository\Tests\Fixtures\Scope;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;

class IDScope implements ScopeInterface
{

    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function scope(Builder $builder)
    {
        $builder->where('id', '=', $this->id);
    }
}