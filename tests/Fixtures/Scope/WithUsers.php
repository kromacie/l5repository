<?php namespace Kromacie\L5Repository\Tests\Fixtures\Scope;

use Illuminate\Database\Eloquent\Builder;
use Kromacie\L5Repository\Contracts\ScopeInterface;
use Kromacie\L5Repository\Contracts\TaggableInterface;
use Kromacie\L5Repository\Tests\Fixtures\User;

class WithUsers implements TaggableInterface, ScopeInterface
{

    public function scope(Builder $builder)
    {
        $builder->with('user');
    }

    public function tags(): array
    {
        return [
            User::class
        ];
    }
}