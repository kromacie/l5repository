<?php namespace Kromacie\L5Repository\Tests\Fixtures;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name', 'email'
    ];
}