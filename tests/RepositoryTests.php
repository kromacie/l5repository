<?php namespace Kromacie\L5Repository\Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Kromacie\L5Repository\Commands\ResolvesNames;
use Kromacie\L5Repository\Contracts\ActionInterface;
use Kromacie\L5Repository\Contracts\ResolverInterface;
use Kromacie\L5Repository\Exceptions\ConfigNotFoundException;
use Kromacie\L5Repository\Exceptions\NotCachableException;
use Kromacie\L5Repository\Exceptions\RuntimeException;
use Kromacie\L5Repository\ConfigEditor;
use Kromacie\L5Repository\Generators\ActionGenerator;
use Kromacie\L5Repository\NamespaceFileCreator;
use Kromacie\L5Repository\Generators\RepositoryGenerator;
use Kromacie\L5Repository\NamespaceResolver;
use Kromacie\L5Repository\Repositories\AbstractRepository;
use Kromacie\L5Repository\RepositoryResolver;
use Kromacie\L5Repository\Tests\Fixtures\Action\Count;
use Kromacie\L5Repository\Tests\Fixtures\Action\Exists;
use Kromacie\L5Repository\Tests\Fixtures\Action\RuntimeAction;
use Kromacie\L5Repository\Tests\Fixtures\Action\ShowUser;
use Kromacie\L5Repository\Tests\Fixtures\Action\Sum;
use Kromacie\L5Repository\Tests\Fixtures\Repository\ArticleRepository;
use Kromacie\L5Repository\Tests\Fixtures\Repository\UserRepository;
use Kromacie\L5Repository\Tests\Fixtures\Repository\WalletRepository;
use Kromacie\L5Repository\Tests\Fixtures\Scope\EmailScope;
use Kromacie\L5Repository\Tests\Fixtures\Scope\IDScope;
use Kromacie\L5Repository\Tests\Fixtures\Scope\WithUsers;
use Orchestra\Testbench\TestCase;

class RepositoryTests extends TestCase
{

    use CreatesApplication;

    public function testMigratingTable()
    {
        $this->assertTrue(DB::table('users')->exists());
        $this->assertTrue(DB::table('articles')->exists());
        $this->assertTrue(DB::table('wallets')->exists());
    }

    public function testUserRepository()
    {
        /** @var UserRepository $users */
        $users = $this->app->make(UserRepository::class);

        $user = $users->perform(new ShowUser());

        $this->assertEquals(1, $user->id);
    }

    public function testCachableRepository()
    {
        /** @var UserRepository $users */
        $users = $this->app->make(UserRepository::class);
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $user = $users->perform(new ShowUser());
        $wallet = $wallets->showWallet();

        $wallets->scope(new IDScope(1));
        $users->scope(new IDScope(1));

        $this->assertTrue($resolver->isCachable($wallets, 'showWallet'));
        $this->assertTrue($resolver->cacheHas($wallets, 'showWallet'));
        $this->assertFalse($resolver->cacheHas($users, ShowUser::class));
    }

    public function testOfFlushRepository()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);
        /** @var ArticleRepository $articles */
        $articles = $this->app->make(ArticleRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $wallet = $wallets->showWallet();
        $article = $articles->getAll();

        $wallets->scope(new IDScope(1));

        $resolver->flushRepository($wallets);

        $this->assertFalse($resolver->cacheHas($wallets, 'showWallet'));
        $this->assertTrue($resolver->cacheHas($articles, 'getAll'));

        $articles->flush();

        $this->assertFalse($resolver->cacheHas($articles, 'getAll'));
    }

    public function testOfFlushAll()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);
        /** @var ArticleRepository $articles */
        $articles = $this->app->make(ArticleRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $wallet = $wallets->showWallet();
        $article = $articles->getAll();

        $wallets->scope(new IDScope(1));

        $this->assertTrue($resolver->cacheHas($wallets, 'showWallet'));
        $this->assertTrue($resolver->cacheHas($articles, 'getAll'));

        $resolver->flushAll();

        $this->assertFalse($resolver->cacheHas($wallets, 'showWallet'));
        $this->assertFalse($resolver->cacheHas($articles, 'getAll'));
    }

    public function testOfThrowRuntimeException()
    {
        $this->expectException(RuntimeException::class);

        /** @var WalletRepository $users */
        $wallets = $this->app->make(WalletRepository::class);

        $wallets->runtime('hehe');
    }

    public function testOfCrud()
    {
        /** @var UserRepository $users */
        $users = $this->app->make(UserRepository::class);

        $user_2 = $users->get();

        $users->update([
            'name' => 'Śmieszek'
        ]);
        $users->create([
            'email' => 'hehe@gehe.pl',
            'name' => 'Fajfus'
        ]);


        $users->scope(new EmailScope('hehe@gehe.pl'));

        $paginate = $users->paginate(2, ['*'], 'page', 1);

        $user_3 = $users->first();

        $user_1 = $users->first();

        $users->updateOrCreate([
            'email' => 'hehe1@gehe.pl',
            'name' => 'Fajfus'
        ]);

        $users->updateOrInsert([
            'email' => 'hehe2@gehe.pl',
            'name' => 'Fajfus'
        ]);

        $user_01 = $users->scope(new EmailScope('hehe1@gehe.pl'))->first();
        $user_02 = $users->scope(new EmailScope('hehe2@gehe.pl'))->first();

        $users->delete();

        $this->assertEquals('hehe@gehe.pl', $user_3->email);
        $this->assertEquals('Śmieszek', $user_1->name);
        $this->assertEquals('hehe1@gehe.pl', $user_01->email);
        $this->assertEquals('hehe2@gehe.pl', $user_02->email);
        $this->assertInstanceOf(Collection::class, $user_2);
        $this->assertInstanceOf(LengthAwarePaginator::class, $paginate);
        $this->assertNull($users->first());

    }

    public function testOfFlushRelatedTable()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);
        /** @var ArticleRepository $articles */
        $articles = $this->app->make(ArticleRepository::class);

        $wallet = $wallets->showWalletWithUser();

        $article = $articles->getAll();

        /** @var UserRepository $users */
        $users = $this->app->make(UserRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $wallets->scope(new IDScope(1));
        $wallets->scope(new WithUsers());
        $this->assertTrue($resolver->cacheHas($wallets, 'showWalletWithUser'));

        $users->delete();

        $this->assertFalse($resolver->cacheHas($wallets, 'showWalletWithUser'));
        $this->assertTrue($resolver->cacheHas($articles, 'getAll'));

    }

    public function testOfPaginate()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $page_1 = $wallets->showSite(1);
        $page_2 = $wallets->showSite(2);

        $page_1->firstItem();

        $page_2->firstItem();

        $this->assertNotEquals($page_1, $page_2);

    }

    public function testOfRuntime()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $wallets->perform(new RuntimeAction());

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $this->assertFalse($resolver->cacheHas($wallets, RuntimeAction::class));

    }

    public function testOfGetNotExistsConfig()
    {
        $this->expectException(ConfigNotFoundException::class);

        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $wallets->getConfig('sdf');
    }

    public function testOfCacheHasNotCachableRepository()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(UserRepository::class);
        $users = $this->app->make(WalletRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $this->assertFalse($resolver->cacheHas($wallets, 'something'));
        $this->assertFalse($resolver->cacheHas($users, 'something'));
    }

    public function testOfFlushNotCachableRepository()
    {
        $this->expectException(NotCachableException::class);
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(UserRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $resolver->flushRepository($wallets);
    }

    public function testOfIsCachable()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(UserRepository::class);

        $users = $this->app->make(WalletRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $this->assertFalse($resolver->isCachable($wallets, 'asd'));
        $this->assertFalse($resolver->isCachable($wallets, null));
        $this->assertFalse($resolver->isCachable($users, 'asd'));

        $this->assertTrue($resolver->isCachable($users, 'showWallet'));
        $this->assertTrue($resolver->isCachable($users, 'showSite'));
    }

    public function testOfFlushRelatedWith()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $wallets->showWallet();

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $wallets->scope(new IDScope(1));

        $this->assertTrue($resolver->cacheHas($wallets, 'showWallet'));

        /** @var ArticleRepository $articles */
        $articles = $this->app->make(ArticleRepository::class);

        $articles->getAll();

        $wallets->scope(new WithUsers());

        $resolver->flushRelatedWith($wallets);

        $this->assertFalse($resolver->cacheHas($wallets, 'showWallet'));
        $this->assertTrue($resolver->cacheHas($articles, 'getAll'));

    }

    public function testOfObserver()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $wallet = $wallets->showWallet();

        $wallets->scope(new IDScope(1));
        $this->assertTrue($resolver->cacheHas($wallets, 'showWallet'));

        $wallet->update([
            'amount' => 1
        ]);

        $this->assertFalse($resolver->cacheHas($wallets, 'showWallet'));

        $wallets->flushScopes();

        $wallet = $wallets->showWallet();

        $wallets->scope(new IDScope(1));
        $this->assertTrue($resolver->cacheHas($wallets, 'showWallet'));

        $wallet->delete();

        $this->assertFalse($resolver->cacheHas($wallets, 'showWallet'));

    }

    public function testInsertGetId()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $id = $wallets->insertGetId([
            'user_id' => 1,
            'amount' => 1,
        ]);

        $this->assertIsInt($id);
    }

    public function testExists()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $exists = $wallets->perform(new Exists());

        $this->assertTrue($exists);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $this->assertTrue($resolver->cacheHas($wallets, Exists::class));

        $this->assertEquals($exists, $wallets->perform(new Exists()));
    }

    public function testSum()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $sum = $wallets->perform(new Sum());

        $this->assertTrue($sum > 0);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $this->assertTrue($resolver->cacheHas($wallets, Sum::class));

        $this->assertEquals($sum, $wallets->perform(new Sum()));
    }

    public function testCount()
    {
        /** @var WalletRepository $wallets */
        $wallets = $this->app->make(WalletRepository::class);

        $count = $wallets->perform(new Count());

        $this->assertTrue($count > 0);

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->get(ResolverInterface::class);

        $this->assertTrue($resolver->cacheHas($wallets, Count::class));

        $this->assertEquals($count, $wallets->perform(new Count()));
    }

    public function testTempConfigFileExistence()
    {
        $this->assertTrue(file_exists(__DIR__ . '/config.php'), "File should be created during testing");
    }

    public function testRepositoryWriteToFile()
    {
        $generator = $this->getConfigEditor();
        $generator->push("repositories", WalletRepository::class);

        $configFile = include __DIR__ . "/config.php";

        $this->assertTrue(in_array(WalletRepository::class, $configFile["repositories"]));

        $repository = $configFile["repositories"][0];
        $repository = $this->app->make($repository);

        $this->assertInstanceOf(WalletRepository::class, $repository);
    }

    public function testNamespaceResolver()
    {
        $resolver = $this->getNamespaceResolver();

        $this->assertEquals('Kromacie\\L5Repository\\Tests\\Fixtures\\Generated\\Repository\\Wallets', $resolver->forRepository('Wallets/WalletRepository', $repository));
        $this->assertEquals('Kromacie\\L5Repository\\Tests\\Fixtures\\Generated\\Action\\Wallets', $resolver->forAction('Wallets/Create', $action));
        $this->assertEquals('Kromacie\\L5Repository\\Tests\\Fixtures\\Generated\\Model', $resolver->forModel('Wallet', $model));

        $this->assertEquals('WalletRepository', $repository);
        $this->assertEquals('Create', $action);
        $this->assertEquals('Wallet', $model);
    }

    public function testFileCreator()
    {
        $creator = $this->getFileCreator();

        $file = $creator->create('WalletRepository', 'Kromacie\\L5Repository\\Tests\\Fixtures\\Generated\\Repository', "<?php\n\nreturn ['data' => 10];");

        $this->assertFileExists($file);

        $array = include $file;

        $this->assertEquals(10, $array['data']);

        unlink($file);
    }

    public function testRepositoryGenerator()
    {
        $generator = $this->getRepositoryGenerator();

        $file = $generator->generate("WalletRepository","Wallet", true);

        include $file;

        $class = $this->app->make('\Kromacie\L5Repository\Tests\Fixtures\Generated\Repository\WalletRepository');

        $this->assertInstanceOf(AbstractRepository::class, $class);

        unlink($file);
    }

    public function testActionGenerator()
    {
        $generator = $this->getActionGenerator();

        $file = $generator->generate("Wallets\\Createa");

        $this->assertFileExists($file);

        include $file;

        $action = $this->app->make('\Kromacie\L5Repository\Tests\Fixtures\Generated\Action\Wallets\Createa');

        $this->assertInstanceOf(ActionInterface::class, $action);

        unlink($file);
    }

    public function testActionGenerateWithPrefix()
    {
        $generator = $this->getActionGenerator();

        $file = $generator->generate("Createb", "Wallets");

        $this->assertFileExists($file);

        include $file;

        $action = $this->app->make('\Kromacie\L5Repository\Tests\Fixtures\Generated\Action\Wallets\Createb');

        $this->assertInstanceOf(ActionInterface::class, $action);

        unlink($file);
    }

    public function testRepositoryScopeAsClosure()
    {
        /** @var WalletRepository $repository */
        $repository = $this->app->make(WalletRepository::class);

        $repository->scope(function (\Illuminate\Database\Eloquent\Builder $builder){
            $builder->where('id', '=', 2);
        });

        $this->assertNull($repository->showWallet());

        /** @var RepositoryResolver $resolver */
        $resolver = $this->app->make(RepositoryResolver::class);

        $this->assertTrue($resolver->isCachable($repository, 'showWallet'));

        $repository->scope(function (){

        });

        $this->assertFalse($resolver->isCachable($repository, 'showWallet'));
    }

    public function testResolvesNamesTrait()
    {
        $class = new class {
            use ResolvesNames;
        };

        $this->assertEquals('WalletRepository', $class->repositoryName('Wallet'));
        $this->assertEquals('Wallet\\Option', $class->actionName('WalletOptionRepository'));
    }

    public function testCreateRepositoryCommand()
    {
        $this->app->extend(RepositoryGenerator::class, function (){
           return $this->getRepositoryGenerator();
        });

        $this->app->extend(ActionGenerator::class, function (){
            return $this->getActionGenerator();
        });

        /** @var Kernel $kernel */
        $kernel = $this->app[Kernel::class];

        $kernel->call('repository:create', [
            'name' => 'RandomWithSomethingRepository',
            '--cache' => true,
            '--crud' => true,
            '--actions_dir' => 'Random/Something/With'
        ]);

        $this->assertFileExists(__DIR__ . '\Fixtures\Generated\Repository\RandomWithSomethingRepository.php');
        $this->assertFileExists(__DIR__ . '\Fixtures\Generated\Action\Random\Something\With\Create.php');
        $this->assertFileExists(__DIR__ . '\Fixtures\Generated\Action\Random\Something\With\Update.php');
        $this->assertFileExists(__DIR__ . '\Fixtures\Generated\Action\Random\Something\With\Delete.php');
        $this->assertFileExists(__DIR__ . '\Fixtures\Generated\Action\Random\Something\With\Show.php');
        $this->assertFileExists(__DIR__ . '\Fixtures\Generated\Action\Random\Something\With\ShowAll.php');

        $configFile = include __DIR__ . "/config.php";
        include __DIR__ . '\Fixtures\Generated\Repository\RandomWithSomethingRepository.php';

        $this->assertTrue(in_array('Kromacie\\L5Repository\\Tests\\Fixtures\\Generated\\Repository\\RandomWithSomethingRepository', $configFile["repositories"]));

        $this->assertInstanceOf(AbstractRepository::class, $this->app->make($configFile['repositories'][0]));
    }

    public function getNamespaceResolver()
    {
        return new NamespaceResolver(
            __DIR__ . '/Fixtures/Generated/Repository',
            __DIR__ . '/Fixtures/Generated/Action',
            __DIR__ . '/Fixtures/Generated/Model',
            __DIR__,
            'Kromacie\\L5Repository\\Tests');
    }

    public function getFileCreator()
    {
        return new NamespaceFileCreator(__DIR__, 'Kromacie\\L5Repository\\Tests');
    }

    public function getConfigEditor()
    {
        return new ConfigEditor(__DIR__ . "/config.php");
    }

    public function getRepositoryGenerator()
    {
        return new RepositoryGenerator($this->getNamespaceResolver(), $this->getFileCreator(), $this->getConfigEditor());
    }

    public function getActionGenerator()
    {
        return new ActionGenerator($this->getNamespaceResolver(), $this->getFileCreator());
    }

}