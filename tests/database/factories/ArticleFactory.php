<?php namespace Kromacie\L5Repository\Tests;

use Faker\Generator as Faker;
use Kromacie\L5Repository\Tests\Fixtures\Article;
use Kromacie\L5Repository\Tests\Fixtures\User;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->paragraphs(3, true),
        'user_id' => factory(User::class)->create()->id
    ];
});