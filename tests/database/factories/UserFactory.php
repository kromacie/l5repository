<?php namespace Kromacie\L5Repository\Tests;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Kromacie\L5Repository\Tests\Fixtures\User;

/** @var Factory $factory */
$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
    ];
});