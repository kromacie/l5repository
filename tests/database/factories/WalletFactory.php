<?php namespace Kromacie\L5Repository\Tests;

use Faker\Generator as Faker;
use Kromacie\L5Repository\Tests\Fixtures\User;
use Kromacie\L5Repository\Tests\Fixtures\Wallet;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Wallet::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'amount' => $faker->numberBetween(10, 1000)
    ];
});